package org.sample.project.demo.interfaces.dto

data class SampleResult(
    val message: String
)
