package org.sample.project.demo.interfaces

import org.sample.project.demo.interfaces.dto.SampleResult
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono

@RestController
@RequestMapping("/v1")
class WebController() {

    @RequestMapping(
        value = ["/"],
        produces = ["application/json"],
        consumes = ["*/*"],
        method = [RequestMethod.GET]
    )
    fun sampleRequest(): Mono<SampleResult> {
        val result = SampleResult("This is sample response")
        return Mono.justOrEmpty(result)
    }

    @RequestMapping(
        value = ["/healthz"],
        produces = ["application/json"],
        consumes = ["*/*"],
        method = [RequestMethod.GET]
    )
    fun healthCheck(): Mono<SampleResult> {
        val result = SampleResult("This is health check")
        return Mono.justOrEmpty(result)
    }
}
