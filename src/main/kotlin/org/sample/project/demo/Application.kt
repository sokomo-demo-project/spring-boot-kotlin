package org.sample.project.demo

import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class Application

fun main(args: Array<String>) {
    val logger = LoggerFactory.getLogger(Application::class.java)
    logger.info("Starting Demo Service ...")
    runApplication<Application>(*args)
    logger.info("Demo Service started ...")
}
